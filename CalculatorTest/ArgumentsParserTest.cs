﻿using SimpleCalculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CalculatorTest
{
    [TestClass]
    public class OperationArgumentsParserTest
    {
        private OperationArgumentsParser _parser;

        private static readonly string[] _validExpressions = new string[]
        {
            "6+5", "6.0+4.6", "5.+10.", ".5+.1", "6+5.0", "-5+-10", "-6.0+-10.5", " 6 + 0.3 ",
            "6-5", "6.0-4.6", "5.-10.", ".5-.1", "6-5.0", "-5--10", "-6.0--10.5", " 6 - 0.3 ",
            "6*5", "6.0*4.6", "5.*10.", ".5*.1", "6*5.0", "-5*-10", "-6.0*-10.5", " 6 * 0.3 ",
            "6/5", "6.0/4.6", "5./10.", ".5/.1", "6/5.0", "-5/-10", "-6.0/-10.5", " 6 / 0.3 ",
            "root(4,2)", "root(4.0,2.0)", "root(4.,2.)", "root(.4,.2)", "root(4,2.0)", "root(-4,-2)", "root(-40.0,-2.0)", " root( 4 , 2.0 ) ",
            "pow(4,2)", "pow(4.0,2.0)", "pow(4.,2.)", "pow(.4,.2)", "pow(4,2.0)", "pow(-4,-2)", "pow(-40.0,-2.0)", " pow( 4 , 2.0 ) "
        };

        [TestInitialize]
        public void Initialize()
        {
            _parser = new OperationArgumentsParser();
        }

        [TestMethod]
        public void Parse_ValidExpression_ReturnsTrue()
        {
            foreach(string expression in _validExpressions)
            {
                Assert.IsTrue(_parser.Parse(expression));
            }
        }
    }
}
