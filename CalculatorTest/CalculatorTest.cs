﻿using SimpleCalculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace CalculatorTest
{
    [TestClass]
    public class CalculatorTest
    {
        private Calculator _calculator;
        private static readonly double _delta = .1e-10;

        [TestInitialize]
        public void Initialize()
        {
            _calculator = new Calculator();
        }

        [TestMethod]
        public void Add_NumericalArguments_ReturnsSum()
        {
            Assert.AreEqual(12, _calculator.Add(7, 5.0), _delta);
        }

        [TestMethod]
        public void Subtract_NumericalArguments_ReturnsResultOfSubtraction()
        {
            Assert.AreEqual(34.5, _calculator.Subtract(35, 0.5), _delta);
        }

        [TestMethod]
        public void Multipy_NumericalArguments_ReturnsProduct()
        {
            Assert.AreEqual(100, _calculator.Multiply(25, 4), _delta);
        }

        [TestMethod]
        public void Divide_NumericalArguments_ReturnsQuotient()
        {
            Assert.AreEqual(5, _calculator.Divide(25, 5.0), _delta);
        }

        [TestMethod]
        [ExpectedException(typeof(DivideByZeroException))]
        public void Divide_DivisorIsZero_ThrowsDivideByZeroException()
        {
            double quotient =_calculator.Divide(10, 0);
        }

        [TestMethod]
        public void Root_NumericalArguments_ReturnsNthRoot()
        {
            // Doubles are not accurate so we need delta argument.
            Assert.AreEqual(5.0, _calculator.Root(125, 3), _delta);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Root_ArgumentIsNegativeAndDegreeIsEven_ThrowsInvalidOperationException()
        {
            double root = _calculator.Root(-4, 2.0);
        }

        [TestMethod]
        public void Power_NumericalArguments_ReturnsExponentiation()
        {
            Assert.AreEqual(27, _calculator.Power(3.0, 3), _delta);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Power_BaseIsNegativeAndExponentIsEvenFraction_ThrowsInvalidOperationException()
        {
            double exponentiation = _calculator.Power(-4, .5);
        }
    }
}
