﻿using System;
using System.Globalization;

namespace SimpleCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            if(args.Length < 1)
            {
                Console.WriteLine("USAGE: {0} <A><+|-|*|/><B> | root(<A>,<B>) | pow(<A>,<B>)\nwhere <A> and <B> are real numbers.",
                    AppDomain.CurrentDomain.FriendlyName);
                return;
            }

            OperationArgumentsParser parser = new OperationArgumentsParser();
            if(!parser.Parse(args[0]))
            {
                Console.WriteLine("Invalid operation syntax.");                
                return;
            }

            try
            {
                Calculator calculator = new Calculator();
                double result = double.NaN;

                switch(parser.Operation)
                {
                    case OperationType.Addition:
                        result = calculator.Add(parser.A, parser.B);
                        break;

                    case OperationType.Subtraction:
                        result = calculator.Subtract(parser.A, parser.B);
                        break;

                    case OperationType.Multiplication:
                        result = calculator.Multiply(parser.A, parser.B);
                        break;

                    case OperationType.Division:
                        result = calculator.Divide(parser.A, parser.B);
                        break;

                    case OperationType.Root:
                        result = calculator.Root(parser.A, parser.B);
                        break;

                    case OperationType.Exponentiation:
                        result = calculator.Power(parser.A, parser.B);
                        break;
                }

                // Default rounding is so called "banker's rounding" - rounds half to even.
                Console.WriteLine("RESULT = {0}", Math.Round(result, 2, MidpointRounding.AwayFromZero).ToString(CultureInfo.InvariantCulture));
            }
            catch(DivideByZeroException e)
            {
                Console.WriteLine("Division by zero.");
                return;
            }
            catch(InvalidOperationException e)
            {
                Console.WriteLine("Invalid mathematical operation.");
                return;
            }
        }
    }
}
