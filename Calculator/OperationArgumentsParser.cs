﻿using System.Globalization;
using System.Text.RegularExpressions;

namespace SimpleCalculator
{
    public enum OperationType
    {
        Addition, Subtraction, Multiplication, Division, Root, Exponentiation
    }

    public class OperationArgumentsParser
    {
        private const string _regexBasicOperationsPattern = @"^(-{0,1}\d+\.\d+|-{0,1}\d+\.|-{0,1}\.\d+|-{0,1}\d+)(\+|-|\*|\/)(-{0,1}\d+\.\d+|-{0,1}\d+\.|-{0,1}\.\d+|-{0,1}\d+)";
        private const string _regexRootOperationPattern = @"^root\((-{0,1}\d+\.|-{0,1}\.\d+|-{0,1}\d+\.\d+|-{0,1}\d+),(-{0,1}\d+\.|-{0,1}\.\d+|-{0,1}\d+\.\d+|-{0,1}\d+)\)";
        private const string _regexPowerOperationPattern = @"^pow\((-{0,1}\d+\.|-{0,1}\.\d+|-{0,1}\d+\.\d+|-{0,1}\d+),(-{0,1}\d+\.|-{0,1}\.\d+|-{0,1}\d+\.\d+|-{0,1}\d+)\)";

        public OperationType Operation { get; private set; }
        
        public double A { get; private set; }

        public double B { get; private set; }

        public bool Parse(string expression)
        {            
            expression = expression.Replace(" ", string.Empty);

            if (ParseBasicOperationsExpression(expression))
                return true;

            if (ParseRootExpression(expression))
                return true;

            if (ParsePowerExpression(expression))
                return true;

            return false;
        }

        private bool ParseBasicOperationsExpression(string expression)
        {
            Regex regex = new Regex(_regexBasicOperationsPattern);
            Match match = regex.Match(expression);

            if (match.Success)
            {
                A = double.Parse(match.Groups[1].Value, CultureInfo.InvariantCulture);
                B = double.Parse(match.Groups[3].Value, CultureInfo.InvariantCulture);

                switch (match.Groups[2].Value)
                {
                    case "+":
                        Operation = OperationType.Addition;
                        break;

                    case "-":
                        Operation = OperationType.Subtraction;
                        break;

                    case "*":
                        Operation = OperationType.Multiplication;
                        break;

                    case "/":
                        Operation = OperationType.Division;
                        break;
                }

                return true;
            }

            return false;
        }

        private bool ParseRootExpression(string expression)
        {
            Regex regex = new Regex(_regexRootOperationPattern);
            Match match = regex.Match(expression);

            if (match.Success)
            {
                A = double.Parse(match.Groups[1].Value, CultureInfo.InvariantCulture);
                B = double.Parse(match.Groups[2].Value, CultureInfo.InvariantCulture);
                Operation = OperationType.Root;

                return true;
            }

            return false;
        }

        private bool ParsePowerExpression(string expression)
        {
            Regex regex = new Regex(_regexPowerOperationPattern);
            Match match = regex.Match(expression);

            if (match.Success)
            {
                A = double.Parse(match.Groups[1].Value, CultureInfo.InvariantCulture);
                B = double.Parse(match.Groups[2].Value, CultureInfo.InvariantCulture);
                Operation = OperationType.Exponentiation;

                return true;
            }

            return false;
        }
    }
}
