﻿using System;

namespace SimpleCalculator
{
    public class Calculator
    {
        public double Add(double a, double b)
        {
            return a + b;
        }

        public double Subtract(double a, double b)
        {
            return a - b;
        }

        public double Multiply(double a, double b)
        {
            return a * b;
        }

        public double Divide(double a, double b)
        {
            // Division double by zero returns Infinity.
            if (b == 0)
                throw new DivideByZeroException();

            return a / b;
        }

        public double Root(double a, double degree)
        {
            double root = Math.Pow(a, 1.0 / degree);

            if (double.IsNaN(root))
                throw new InvalidOperationException();

            return root;
        }

        public double Power(double a, double exponent)
        {
            double exponentiation = Math.Pow(a, exponent);

            if (double.IsNaN(exponentiation))
                throw new InvalidOperationException();

            return exponentiation;
        }
    }
}
